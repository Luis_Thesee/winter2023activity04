public class Student{
	private String name;
	private int grade;
	private int ammountLearnt;


	public Student(String name){
		this.name = name;
		this.grade = 1;
		this.ammountLearnt = 0;
	}

	public String getName(){
		return this.name;
	}
	
	public int getGrade(){
		return this.grade;
	}
	
	public int getAmmountLearnt(){
		return this.ammountLearnt;
	}
	
	
	
	public void setGrade(int grade){
		this.grade = grade;
	}
	


	
	public void learn(int amountStudied){
		
		if (amountStudied > 0){
			this.ammountLearnt = this.ammountLearnt + amountStudied;
		}
			
	}



}